import { Component } from '@angular/core';

class Item {
    purchase: string;

    constructor(purchase: string) {
        this.purchase = purchase;
    }
}
class ModelType {
    type: string;
    model: string;

    constructor(type: string, model: string) {
        this.type = type;
        this.model = model;
    }
}

@Component({
    selector: 'purchase-app',
    template: `<div class="page-header">
        <h1> Фотоапарати </h1>
    </div>
    <div class="panel">
        <div class="form-inline">
            <div class="form-group">
                <div class="col-md-8">
                    <input class="form-control" [(ngModel)]="textCamera" placeholder = "Название" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-8">
                    <button class="btn btn-default" (click)="addCamera(textCamera)">Додати</button>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Название</th>
                </tr>
            </thead>
            <tbody>
                <tr *ngFor="let item of cameras">
                    <td>{{item.purchase}}</td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="page-header">
        <h1> Типи </h1>
    </div>
    <div class="panel">
        <div class="form-inline">
            <div class="form-group">
                <div class="col-md-8">
                    <input class="form-control" [(ngModel)]="textType" placeholder = "Название" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-8">
                    <button class="btn btn-default" (click)="addType(textType)">Додати</button>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Название</th>
            </tr>
            </thead>
            <tbody>
            <tr *ngFor="let item of types">
                <td>{{item.purchase}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <div class="page-header">
        <h1> Тип & Модель </h1>
    </div>
    <div class="panel">
        <div class="form-inline">
            <div class="form-group">
                <div class="col-md-8">
                    <input class="form-control" [(ngModel)]="type" placeholder = "Тип" />
                    <input class="form-control" [(ngModel)]="model" placeholder = "Модель" />
                </div>
            </div>
            <div class="form-group">
                <div class="col-md-offset-2 col-md-8">
                    <button class="btn btn-default" (click)="addTypeModel(type, model)">Додати</button>
                </div>
            </div>
        </div>
        <table class="table table-striped">
            <thead>
            <tr>
                <th>Название</th>
            </tr>
            </thead>
            <tbody>
            <tr *ngFor="let item of typesModels">
                <td>{{item.type}}</td>
                <td>{{item.model}}</td>
            </tr>
            </tbody>
        </table>
    </div>`
})
export class AppComponent {
    cameras: Item[] =
        [
            { purchase: "Canon"},
            { purchase: "Nikon"}
        ];
    types: Item[] =
        [
            { purchase: "dsr"},
        ];
    typesModels: ModelType[] =
        [
            { type: "dsr", model: "Canon"},
        ];
    addCamera(text: string): void {

        if (text == null || text.trim() == "")
            return;
        this.cameras.push(new Item(text));
    };
    addType(text: string): void {

        if (text == null || text.trim() == "")
            return;
        this.types.push(new Item(text));
    };
    addTypeModel(type: string, model: string): void {

        if (type == null || type.trim() == "" || model == null || model.trim() == "")
            return;
        this.typesModels.push(new ModelType(type, model));
    }
}
